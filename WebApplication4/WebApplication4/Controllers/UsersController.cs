﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class UsersController : Controller
    {
        private otokritikEntities db = new otokritikEntities();

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void Giris(String userName, String password)
        {
            string returnUrl = "/Home/Index/";
            List<user> list = db.user.Where(u => u.username == userName).Where(u=> u.pass == password).ToList();
            if (list.Count != 0)
            {
                Session["Giris"] = "1";
                Session["activeUserName"] = userName;
            }
            else
                Session["Giris"] = null;

            Response.Redirect(returnUrl);

        }
        [HttpPost]
        public void Cikis()
        {
            string returnUrl = "/Home/Index/";
            Session.Remove("Giris");
            Session.Remove("activeUserName");
            Response.Redirect(returnUrl);
        }

        [HttpPost]
        public void Kaydol(String uyeAdi,String mail,String sifre, String sifre2,String adi,String soyadi,String secenek)
        {            
            
            string returnUrl = "/Users/Index/";

           

            try
            {
                if(uyeAdi.Length < 3)
                {                    
                    Session["uyari"] = "Kullanıc adı en az 3 karakter olmalı"; 
                }
                else if(mail.Length == 0 )
                {
                    Session["uyari"] = "Zorunlu alan eposta";
                }
                else if (!mail.Contains("@"))
                {
                    Session["uyari"] = "Geçerli bir eposta adresi giriniz";
                }
                else if( sifre.Length < 5)
                {
                    Session["uyari"] = "Şifre en az 5 karakterli olmalı";
                }
                else if (!sifre2.Equals(sifre))
                {
                    Session["uyari"] ="Şifreler birbirinin aynısı değil" ;
                }
                else if (adi.Length == 0)
                {
                    Session["uyari"] = "Ad en az 3 karakterli olmalı";
                }
                else if (soyadi.Length == 0)
                {
                    Session["uyari"] = "Soyad en az 3 karakterli olmalı";
                }
                

                List <user> list = db.user.Where(u => u.username == uyeAdi).ToList();
                if (list.Count != 0)
                    Session["uyari"] = "Bu üye adı sistemimizde mevcuttur!";
                
                
                user users = new user();
                users.username = uyeAdi;
                users.mail = mail;
                users.pass = sifre;
                users.name = adi;
                users.surname = soyadi;
                db.user.Add(users);
                db.SaveChanges();
                Response.Redirect(returnUrl);
            }
            catch (Exception ex)
            {
                
                Response.Redirect(returnUrl + "?error=ex");
            }
        }

        // GET: Users/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,username,name,surname,used1carid,pass,mail,picturepath,role")] user user)
        {
            if (ModelState.IsValid)
            {
                db.user.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,username,name,surname,used1carid,pass,mail,picturepath,role")] user user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            user user = db.user.Find(id);
            db.user.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
